# AED-Guia3
Actividades correspondientes a la guia 2 de Algortimo y Estructura de Datos.

# Guia3-UI
Tema: Listas enlazadas simples

>Programa que solicite e ingrese enteros a una lista enlazada simple ordenada, y complete la secuencia ingresada. 

	- Crear lista (y nodos)
	- Agregar elementos (nodos)
	- Ordenar elementos
	- Completar valores intermedios (comparacion nodo actual y siguiente)
	- Imprimir lista solicitada

# Obtención del Programa
Clonar repositorio, ingresar a la carpeta clonada y a la carpeta '1'. Ejecutar por terminal:
```
g++ programa.cpp Lista.cpp -o programa
make
./programa
```

# Acerca de

El programa presenta inicialmente un menú con 4 opciones. 

La primera consiste en agregar elementos (numeros enteros) a la lista inicializada al momento de ejecutar el programa. La segunda opción permite la visualizacion de los elementos ingresados por el usuario de forma ascendente (de menor a mayor). Mientras que la tercera opcion permite la modificacion de la lista, llenandola con los enteros sucesores que no se ingresaron en la lista. Finalmente ,la cuarta opción le permite al usuario cerrar el programa.

# Requisitos
- Sistema operativo Linux
- Herramienta de gestion de dependencias make (para Makefile)
- Compilador GNU C++ (g++)

# Construccion
Construido y probado con:
- Ubuntu 18.04.03 LTS
- gcc y g++ version 7.4.0
- GNU Make 4.1
- Editor utilizado: Sublime Text

# Autor
Constanza Valenzuela
