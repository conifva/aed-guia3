#include <iostream>
using namespace std;
/* Estructura de nodo */
#include "programa.h"

#ifndef LISTA_H
#define LISTA_H

class Lista{
	private:
		Nodo *raiz = NULL;
		Nodo *ultimo = NULL;

	public:
		/* constructor*/
		Lista();
		
	
		/* crea un nuevo nodo, recibe instancia */
		void creaNodo (int num);
		void add_elem (Nodo *lista, int elem);
		void ordenarLista ();
		void completarLista ();

		/* imprime la lista. */
		void verLista ();

};
#endif
