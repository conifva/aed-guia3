#include <iostream>
using namespace std;
/* clases */
#include "Lista.h"
/* definición de la estructura Nodo. */
//#include "programa.h"

void menu(Lista *lista){
	int opcion;
	
	cout << "\n\t\tMENU\n" << endl;
	cout << " [1] Agregar numero" << endl;
	cout << " [2] Ver Lista Ordenada" << endl;
	cout << " [3] Completar Lista" << endl;
	cout << " [4] Salir" << endl;
	
	cout << "\n > Ingrese su opción: ";
	cin >> opcion;

	while(getchar() != '\n');

	switch(opcion){
		case 1:
			int num;

			cout << "\n > Ingrese un numero: ";
			cin >> num;

			lista->creaNodo(num);
			lista->ordenarLista();
			/* return a menu */
			menu(lista);
			break;

		case 2:
			cout << "\n\t Lista Ordenada" << endl;
			lista->verLista();
			menu(lista);
			break;
		
		case 3:
			cout << "\n\t Lista Autocompletada" << endl;
			//lista->orderarLista();
			lista->completarLista();
			lista->verLista();
			menu(lista);
			break;

		case 4:
			exit(1);
			break;

		default:
			cout << "Opcion no valida" << endl;
			menu(lista);
			break;
	}
}

/* función principal. */
int main () {
	/* crea lista */
	Lista *lista = new Lista();
	/* mostrar menu */
	menu(lista);

	return 0;
}
