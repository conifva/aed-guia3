# AED-Guia3
Actividades correspondientes a la guia 2 de Algortimo y Estructura de Datos.

# Guia3-UI
Tema: Listas enlazadas simples

>Programa que solicite e ingrese enteros a una lista enlazada simple ordenada.  

	- Crear lista (y nodos)
	- Agregar elementos (nodos)
	- Ordenar elementos 
	- Imprimir lista

# Obtención del Programa
Clonar repositorio, ingresar a la carpeta clonada y a la carpeta '1'. Ejecutar por terminal:
```
g++ programa.cpp Lista.cpp -o programa
make
./programa
```

# Acerca de

El programa presenta inicialmente un menú con 3 opciones. 

La primera consiste en agregar elementos (numeros enteros) a la lista inicializada al momneto de ejecutar el programa. La segunda opción permite la visualizacion de los elemtos ingresados por el usuario de forma ascendente (de menor a mayor). Finalmente la tercera opción le permite al usuario cerrar el programa.

# Requisitos
- Sistema operativo Linux
- Herramienta de gestion de dependencias make (para Makefile)
- Compilador GNU C++ (g++)

# Construccion
Construido y probado con:
- Ubuntu 18.04.03 LTS
- gcc y g++ version 7.4.0
- GNU Make 4.1
- Editor utilizado: Sublime Text

# Autor
Constanza Valenzuela
