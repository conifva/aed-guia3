#include <iostream>
using namespace std;
/* clases */
#include "Lista.h"
/* definición de la estructura Nodo. */
//#include "programa.h"

void menu(Lista *lista){
    int opcion;
    
    cout << "\n\t\tMENU\n" << endl;
    cout << " [1] Agregar numero" << endl;
    cout << " [2] Ver Lista" << endl;
    cout << " [3] Salir" << endl;
    
    cout << "\n > Ingrese su opción: ";
    cin >> opcion;

    while(getchar() != '\n');

    switch(opcion){
        case 1:
            int num;

            cout << "\n > Ingrese un numero: ";
            cin >> num;

            lista->add_list(num);
            /* return a menu */
            menu(lista);
            break;

        case 2:
            cout << "\n\t Lista" << endl;
            lista->ordenarLista();
            lista->verLista();
            menu(lista);
            break;

        case 3:
            exit(1);
            break;

        default:
            cout << "Opcion no valida" << endl;
            break;
    }
}

/* función principal. */
int main () {
    /* crea lista */
    Lista *lista = new Lista();
    /* mostrar menu */
    menu(lista);

    return 0;
}
