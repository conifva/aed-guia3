# AED-Guia3
# Guia3-UI
Actividades tercera guia de AED 2019
con la tematica de ejercitar sobre listas enlazadas simples

# Obtención del Programa
Clonar repositorio, ingresar a la carpeta del ejercicio correspondiente (por ejemplo, "ej1") y ejecutar por terminal:
```
g++ programa.cpp Clase_correspondiente.cpp -o programa
make
./programa
```

# Actividades
1. ej1

	Escriba un programa que solicite números enteros y los vaya ingresando a una lista enlazada simple ordenada. Por cada ingreso debe mostrar el estado de la lista.

2. ej2

	Escriba un programa que genere dos listas enlazadas ordenadas (lea los datos) y forme una tercera lista que resulte de la mezcla de los elementos de ambas listas. Muestre el contenido finalmente de las tres listas.

3. ej3

	Considere que tiene una lista enlazada simple de número enteros, ordenados crecientemente (lea los datos). Donde pueden existir valores no correlativos (ejemplo: 10 - 11 - 15 - 16 - 20).

	Escriba un programa que complete la lista, de tal manera que la misma, una vez modificada almacene todos los valores a partir del número del primer nodo hasta el último número del último nodo.

	Para el ejemplo, la lista guardará los números: 10 - 11 - 12 - 13 - 14 - 15 - 16 - 17 - 18 - 19 - 20.

# Requisitos
- Sistema operativo Linux
- Herramienta de gestion de dependencias make (para Makefile)
- Compilador GNU C++ (g++)

# Construccion
Construido y probado con:
- Ubuntu 18.04.03 LTS
- gcc y g++ version 7.4.0
- GNU Make 4.1
- Editor utilizado: Sublime Text

# Autor
Constanza Valenzuela
