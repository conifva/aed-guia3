#include <iostream>
using namespace std;
/* Definición de la clase */
#include "Lista.h"

Lista::Lista(){}

Nodo* Lista:: getRaiz(){
	return this->raiz;
}

void Lista::crearNodo(int num){
	Nodo *tmp;

	/* crea un nodo */
	tmp = new Nodo;
	/* asigna al nodo sig */
	tmp->elem = num;
	/* apunta a NULL por defecto */
	tmp->sig = NULL;

	/* si el es primer nodo de la lista, lo deja como raíz y como último nodo. */
	if (this->raiz == NULL) { //si la lista esta vacia
		this->raiz = tmp;
		this->ultimo = this->raiz;
	/* de lo contrario, apunta el actual último nodo al nuevo y deja el nuevo como el último de la lista. */
	} else { //lista tiene elementos
		this->ultimo->sig = tmp;
		this->ultimo = tmp;
	}
	ordenarLista(this->raiz);
}
 
void Lista::ordenarLista(Nodo *tmp){
	int puntero; //auxiliar

	/* recorre lista mientras q el sucesor del primer elem sea nulo */
	while (tmp != NULL){
		Nodo *tmp2 = tmp->sig;
		/* recorre lista mientras el nodo sucesor (tmp2) no sea nulo */
		while (tmp2 != NULL) {
			/* si el primer nodo es mayor que el sucesor */
			if (tmp->elem > tmp2->elem) {
				/* auxiliar (puntero) guarda el nodo sucesor */
				puntero = tmp2->elem;
				/* sucesor para a ser el nodo anterior */
				tmp2->elem = tmp->elem;
				/* primer nodo toma valor del auxiliar */
				tmp->elem = puntero;
			}
			/* avanza de nodo para nueva comparacion */
			tmp2 = tmp2->sig;
		}
		/* avanza de nodo para nueva comparacion */
		tmp = tmp->sig;
	}
	
}

void Lista::verLista () {   //recorrer lista
	/* utiliza variable tmporal para recorrer la lista. */
	Nodo *tmp = this->raiz;

	if (!tmp) {
		cout << "\n La Lista está vacía " << endl;
	} else {
		/* la recorre mientras sea distinto de NULL (no hay más nodos). */
		while (tmp != NULL) {
			
			cout << tmp->elem << " -> ";
			if (!tmp->sig){
				cout << "NULL" << endl;
				tmp = tmp->sig;
			} else {
				tmp = tmp->sig;
			}
		}
	}
}