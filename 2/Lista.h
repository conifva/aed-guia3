#include <iostream>
using namespace std;
/* Estructura de nodo */
#include "programa.h"

#ifndef LISTA_H
#define LISTA_H

class Lista{
	private:
		Nodo *raiz = NULL;
		Nodo *ultimo = NULL;
		/*int num;
		Nodo *lista = NULL;*/

	public:
		/* constructor*/
		Lista();
		
		/* crea un nuevo nodo, recibe instancia */
		void crearNodo (int num);
		void ordenarLista (Nodo *tmp);
		/* imprime la lista. */
		void verLista ();

		Nodo* getRaiz();

};
#endif
