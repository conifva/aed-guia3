# AED-Guia3
Actividades correspondientes a la guia 3 de Algortimo y Estructura de Datos.

# Guia3-UI
Tema: Listas enlazadas simples

>Programa que genere dos listas enlazadas ordenadas y forme una tercera lista de los elementos de ambas listas.

	- Crear 3 listas (y nodos)
	- Agregar elementos a lista seleccionada y a la lista combinada
	- Ordenar elementos
	- Imprimir lista

# Obtención del Programa
Clonar repositorio, ingresar a la carpeta clonada y a la carpeta '2'. Ejecutar por terminal:
```
g++ programa.cpp Lista.cpp -o programa
make
./programa
```

# Acerca de

El programa presenta inicialmente un menú con 4 opciones. 

La primera y segunda consiste en agregar elementos (numeros enteros) a una de las 3 listas inicializada al momento de ejecutar el programa, respectivamente, ademas de la visualizacion ordenada de sus elementos en forma ascendente. La tercera opción permite la visualizacion de los elemetos de la tercera lista, correspondiente a los numeros ingresados por el usuario en las 2 listas anteriores de forma ascendente (de menor a mayor). Finalmente la cuarta opción le permite al usuario cerrar el programa.

# Requisitos
- Sistema operativo Linux
- Herramienta de gestion de dependencias make (para Makefile)
- Compilador GNU C++ (g++)

# Construccion
Construido y probado con:
- Ubuntu 18.04.03 LTS
- gcc y g++ version 7.4.0
- GNU Make 4.1
- Editor utilizado: Sublime Text

# Autor
Constanza Valenzuela
