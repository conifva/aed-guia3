#include <iostream>
using namespace std;
/* clases */
#include "Lista.h"

void entrada_elem(Lista *lista, Lista * lista3){
	int elem;

	cout << "\n > Ingrese un elemento: ";
	cin >> elem;
	lista->crearNodo(elem); //crea lista, agrega elemento

	int num;
	/* Combinar listas */
	/* puntero que señala inicio de la lista */
	Nodo *tmp = lista->getRaiz();

	while(tmp != NULL) {
		/* agrega elemen to a la lista combinada */
		num = tmp->elem;
		lista3->crearNodo(num);
		/* senñalar siguiente nodo */
		tmp = tmp->sig;
	}
}

void menu(Lista *lista1, Lista *lista2, Lista *lista3){
	
	int opcion;

	cout << "\n\t\tMENU\n" << endl;
	cout << " [1] Agregar elemento lista A" << endl;
	cout << " [2] Agregar elemento lista B" << endl;
	cout << " [3] Ver Lista Mezclada Generada" << endl;
	cout << " [4] Salir" << endl;
	
	cout << "\n > Ingrese su opción: ";
	cin >> opcion;

	while(getchar() != '\n');

	switch(opcion){
		case 1:
			cout << "\n\t Lista A" << endl;
			entrada_elem(lista1, lista3);
			cout << "\n\t Lista A Ordenada" << endl;
			lista1->verLista(); //mostrar

			/* return a menu */
			menu(lista1, lista2, lista3);
			break;

		case 2:
			cout << "\n\t Lista B" << endl;
			entrada_elem(lista2, lista3);		
			//lista2->crearNodo(elem); //agrega elemento
			cout << "\n\t Lista B Ordenada" << endl;
			lista2->verLista(); //mostrar

			/* return a menu */
			menu(lista1, lista2, lista3);
			break;
			
		case 3:
			cout << "\n\t Lista Mezclada Ordenada" << endl;
			lista3->verLista(); //mostrar
			menu(lista1, lista2, lista3);
			break;

		case 4:
			exit(1);
			break;

		default:
			cout << "Opcion no valida" << endl;
			menu(lista1, lista2, lista3);
			break;
	}
}

/* función principal. */
int main () {
	/* crea lista */
	Lista *lista1 = new Lista();
	Lista *lista2 = new Lista();
	Lista *lista3 = new Lista();

	/* mostrar menu */
	menu(lista1, lista2, lista3);

	return 0;
}
